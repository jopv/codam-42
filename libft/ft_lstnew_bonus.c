/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lstnew_bonus.c                                  :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖        <😖       @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/11/27 19:25:53 by 😖              #+#    #+#                */
/*   Updated: 2019/12/19 16:50:04 by 😖             ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

t_list	*ft_lstnew(void *content)
{
	t_list *toreturn;

	toreturn = malloc(sizeof(t_list));
	if (!toreturn)
		return (NULL);
	toreturn->content = content;
	toreturn->next = NULL;
	return (toreturn);
}
