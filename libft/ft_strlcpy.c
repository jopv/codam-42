/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strlcpy.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖        <😖       @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/11/05 12:16:09 by 😖              #+#    #+#                */
/*   Updated: 2019/12/23 17:52:52 by 😖             ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "libft.h"

size_t	ft_strlcpy(char *dst, const char *src, size_t dstsize)
{
	size_t	i;

	i = 0;
	if (!dst)
		return (0);
	if (dstsize == 0)
		return (ft_strlen(src));
	while (i < (dstsize - 1))
	{
		dst[i] = src[i];
		if ((dst[i] == '\0') && (src[i] == '\0'))
			break ;
		i = i + 1;
	}
	if (i == (dstsize - 1))
	{
		if (dstsize > 0)
			dst[i] = '\0';
		while (src[i] != '\0')
			i = i + 1;
	}
	return (i);
}
