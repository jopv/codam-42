/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strdup.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖        <marvin@codam.nl>                   +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/14 16:59:44 by 😖              #+#    #+#                */
/*   Updated: 2019/11/29 13:08:00 by 😖             ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stddef.h>
#include "libft.h"

char	*ft_strdup(const char *src)
{
	size_t		src_size;
	char		*dest;

	src_size = ft_strlen(src) + 1;
	dest = malloc(src_size);
	if (dest == NULL)
		return (NULL);
	ft_strlcpy(dest, src, src_size);
	return (dest);
}
