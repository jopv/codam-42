/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strlcat.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖        <😖       @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/11/05 12:35:09 by 😖              #+#    #+#                */
/*   Updated: 2019/12/23 17:53:00 by 😖             ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t dstsize)
{
	size_t initiallen;

	initiallen = 0;
	while (dst[initiallen] != '\0' && initiallen < dstsize)
		initiallen = initiallen + 1;
	return (ft_strlcpy(
				&dst[initiallen], src,
				dstsize - initiallen)
				+ initiallen);
}
