/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lstdelone_bonus.c                               :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖        <😖       @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/12/19 13:12:55 by 😖              #+#    #+#                */
/*   Updated: 2019/12/19 16:49:55 by 😖             ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	ft_lstdelone(t_list *todel, void (*del)(void *))
{
	if (!todel)
		return ;
	del(todel->content);
	free(todel);
}
