/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_calloc.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖        <😖       @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/11/26 19:24:38 by 😖              #+#    #+#                */
/*   Updated: 2019/12/02 14:37:56 by 😖             ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	*ft_calloc(size_t count, size_t size)
{
	void *toreturn;

	toreturn = malloc(count * size);
	if (toreturn == NULL)
		return (NULL);
	ft_bzero(toreturn, count * size);
	return (toreturn);
}
