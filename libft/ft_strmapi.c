/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strmapi.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖        <😖       @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/11/21 12:21:26 by 😖              #+#    #+#                */
/*   Updated: 2019/12/18 17:00:28 by 😖             ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned int	i;
	unsigned int	len;
	char			*toreturn;

	if (s == NULL || f == NULL)
		return (NULL);
	i = 0;
	len = (unsigned int)ft_strlen(s);
	toreturn = malloc(len + 1);
	if (toreturn == NULL)
		return (NULL);
	while (i < len)
	{
		toreturn[i] = (*f)(i, s[i]);
		i = i + 1;
	}
	toreturn[i] = '\0';
	return (toreturn);
}
