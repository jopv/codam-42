/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strchr.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖        <😖       @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/10/30 14:53:09 by 😖              #+#    #+#                */
/*   Updated: 2019/12/23 17:51:11 by 😖             ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

char	*ft_strchr(const char *b, int c)
{
	char	*src;
	int		i;

	src = (char *)b;
	i = 0;
	while (src[i] != '\0')
	{
		if (src[i] == c)
		{
			return (&src[i]);
		}
		i = i + 1;
	}
	if (c == '\0')
		return (&src[i]);
	else
		return (NULL);
}
