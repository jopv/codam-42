/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_memset.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖        <😖       @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/10/28 18:21:17 by 😖              #+#    #+#                */
/*   Updated: 2019/12/27 12:04:06 by 😖             ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char	*bpos;
	int				i;

	i = 0;
	bpos = (unsigned char *)b;
	while (len > 0)
	{
		*bpos = c;
		bpos = bpos + 1;
		len = len - 1;
	}
	return (b);
}
