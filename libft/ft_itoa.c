/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_itoa.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖        <😖       @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/11/21 15:58:15 by 😖              #+#    #+#                */
/*   Updated: 2019/12/29 21:33:15 by 😖             ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static	int		ft_itoa_measurelen(int nb)
{
	int		len;
	int		measurelen;

	len = 1;
	measurelen = nb;
	if (measurelen < 0)
	{
		measurelen = -measurelen;
		len = len + 1;
	}
	while (measurelen != 0)
	{
		measurelen = measurelen / 10;
		len = len + 1;
	}
	return (len);
}

static	char	*ft_itoa_makestring(int nb)
{
	char	*toreturn;
	int		i;

	toreturn = malloc(ft_itoa_measurelen(nb));
	if (toreturn == NULL)
		return (NULL);
	i = 0;
	while (nb >= 10)
	{
		toreturn[i] = (nb % 10) + '0';
		nb = nb / 10;
		i = i + 1;
	}
	toreturn[i] = (nb % 10) + '0';
	toreturn[i + 1] = '\0';
	ft_reverse(toreturn);
	return (toreturn);
}

char			*ft_itoa(int nb)
{
	char	*toreturn;
	char	*numstring;
	int		len;

	if (nb == 0)
		return (ft_strdup("0"));
	else if (nb == -2147483648)
		return (ft_strdup("-2147483648"));
	len = ft_itoa_measurelen(nb);
	numstring = ft_itoa_makestring((nb < 0) ? -nb : nb);
	toreturn = ft_calloc(len, sizeof(char));
	if (nb < 0 && toreturn != NULL)
		*toreturn = '-';
	else if (numstring == NULL || toreturn == NULL)
		return (NULL);
	ft_strlcat(toreturn, numstring, len);
	return (toreturn);
}
