/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strrchr.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖        <😖       @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/10/30 15:06:35 by 😖              #+#    #+#                */
/*   Updated: 2019/12/23 17:51:42 by 😖             ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

char	*ft_strrchr(const char *b, int c)
{
	char	*src;
	int		i;
	char	*last;

	src = (char *)b;
	i = 0;
	last = NULL;
	while (src[i] != '\0')
	{
		if (src[i] == c)
		{
			last = &src[i];
		}
		i = i + 1;
	}
	if (c == '\0')
		return (&src[i]);
	else
		return (last);
}
