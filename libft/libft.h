/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   libft.h                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖        <😖       @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/11/01 18:01:57 by 😖              #+#    #+#                */
/*   Updated: 2019/12/23 17:54:00 by 😖             ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <stddef.h>

int					ft_atoi(const char *str);
void				*ft_bzero(void *b, size_t len);
int					ft_isascii(int c);
int					ft_isalnum(int c);
int					ft_isprint(int c);
int					ft_isdigit(int c);
int					ft_isupper(int c);
int					ft_islower(int c);
int					ft_isalpha(int c);
int					ft_toupper(int c);
int					ft_tolower(int c);
void				*ft_memccpy(void *dst, const void *src, int c, size_t len);
void				*ft_memchr(const void *b, int c, size_t len);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
void				*ft_memcpy(void *dst, const void *src, size_t len);
void				*ft_memmove(void *dst, const void *src, size_t len);
void				*ft_memset(void *b, int c, size_t len);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char *str, int fd);
void				ft_putendl_fd(char *s, int fd);
void				ft_putnbr_fd(int nb, int fd);
struct				s_ft_split
{
	int			start;
	int			words;
	int			index;
	const char	*str;
	char		seperator;
	char		**toreturn;
};
char				**ft_split(char const *str, char c);
char				*ft_strchr(const void *b, int c);
char				*ft_strrchr(const void *b, int c);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
char				*ft_strnstr(const char *str, const char *find, size_t len);
size_t				ft_strnlen(const char *s, size_t maxlen);
size_t				ft_strlen(const char *s);
size_t				ft_strlcpy(char *dst, const char *src, size_t dstsize);
char				*ft_strdup(const char *src);
size_t				ft_strlcat(char *dst, const char *src, size_t dstsize);
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
char				*ft_substr(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strtrim(char const *s1, char const *garbage);
void				ft_reverse(char *s);
char				*ft_itoa(int nb);
void				*ft_calloc(size_t count, size_t size);
typedef struct		s_list
{
	void			*content;
	struct s_list	*next;
}					t_list;
t_list				*ft_lstnew(void *content);
void				ft_lstadd_front(t_list **firstel, t_list *newel);
int					ft_lstsize(t_list *firstel);
t_list				*ft_lstlast(t_list *lst);
void				ft_lstadd_back(t_list **firstel, t_list *newel);
void				ft_lstdelone(t_list *todel, void (*del)(void *));
void				ft_lstclear(t_list **firstdel, void (*del)(void *));
void				ft_lstiter(t_list *lst, void (*f)(void *));
t_list				*ft_lstmap(t_list *lst, void *(*f)(void *),
						void (*del)(void *));

#endif
