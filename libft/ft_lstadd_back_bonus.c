/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lstadd_back_bonus.c                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖        <😖       @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/12/19 12:08:28 by 😖              #+#    #+#                */
/*   Updated: 2019/12/30 13:27:13 by 😖             ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_back(t_list **firstel, t_list *newel)
{
	t_list	*currentel;

	if (!newel)
		return ;
	else if (!firstel)
	{
		*firstel = newel;
		return ;
	}
	currentel = *firstel;
	if (!*firstel)
		currentel = newel;
	else
	{
		while (currentel->next)
			currentel = currentel->next;
		currentel->next = newel;
	}
}
