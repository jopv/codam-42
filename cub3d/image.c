/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   image.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/05 14:36:45 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include "mlx.h"
#include <stdlib.h>

t_image			*imagefactory(struct s_mlxvars *s, int x, int y)
{
	t_image *image;

	image = malloc(sizeof(t_image));
	image->img = mlx_new_image(s->mlx, x, y);
	image->addr = mlx_get_data_addr(
		image->img, &(image->bpp), &(image->linelength), &(image->endian));
	image->brush.mlxformat = 0x00FFFFFF;
	image->x = x;
	image->y = y;
	return (image);
}

void			loadtexture(char *path, int face, struct s_mlxvars *s)
{
	t_image *texture;
	t_image **toassign;

	texture = malloc(sizeof(t_image));
	if (texture == NULL)
		quit("Error:\nCouldn't allocate memory for texture", s);
	texture->img = mlx_xpm_file_to_image(
				s->mlx, path, &(texture->x), &(texture->y));
	if (texture->img == NULL)
		texture->img = mlx_png_file_to_image(
			s->mlx, path, &(texture->x), &(texture->y));
	if (texture->img == NULL)
		quit("Error:\nPlease provide valid textures in PNG or XPM3 format.", s);
	texture->addr = mlx_get_data_addr(texture->img,
		&(texture->bpp), &(texture->linelength), &(texture->endian));
	texture->brush.mlxformat = 0x00FFFFFF;
	texture->x = (texture->x <= DATA_TEX_SIZE) ? texture->x : DATA_TEX_SIZE;
	texture->y = (texture->y <= DATA_TEX_SIZE) ? texture->y : DATA_TEX_SIZE;
	toassign = textureforface(face, s);
	if (*toassign != NULL)
		quit("Error:\nTwo textures given for a single face", s);
	*toassign = texture;
}

unsigned int	eyedropper(t_image *image, int x, int y)
{
	if (x > image->x)
		x = x - 2;
	if (y > image->y)
		y = y - 2;
	if (x < 0 || y < 0)
		return (0x00FFFFFF);
	return (*(unsigned int*)(((char*)image->addr) +
		(y * image->linelength + x * (image->bpp / 8))));
}

void			drawpixel(t_image *image, int x, int y)
{
	if (x < 0)
		x = 0;
	if (y < 0)
		y = 0;
	if (x >= image->x)
		x = image->x - 1;
	if (y >= image->y)
		y = image->y - 1;
	*(unsigned int*)(((char*)image->addr) + (y *
		image->linelength + x * (image->bpp / 8))) = image->brush.mlxformat;
}

void			drawverticalline(t_image *image, int x, int start, int length)
{
	int i;

	i = 0;
	if (start < 0)
		start = 0;
	if (length < 0)
		length = 0;
	if (start + length >= image->y)
		length = image->y - start - 1;
	if (x > image->x)
		x = image->x;
	else if (x < 0)
		x = 0;
	while (i < length)
	{
		*(unsigned int*)(((char*)image->addr) + ((i + start) *
			image->linelength + x * (image->bpp / 8))) = image->brush.mlxformat;
		i = i + 1;
	}
}
