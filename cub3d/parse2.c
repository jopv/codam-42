/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   parse2.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/23 23:14:05 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "cub3d.h"

void		multiplespawncheck(char *mapstring, struct s_mlxvars *s)
{
	size_t	i;
	int		spawns;

	i = 0;
	spawns = 0;
	while (i < ft_strlen(mapstring))
	{
		if (ft_strchr("NSWE", mapstring[i]))
			spawns = spawns + 1;
		if (spawns > 1)
			quit("Error:\nMultiple spawn points in map.", s);
		i = i + 1;
	}
	if (spawns == 0)
		quit("Error:\nNo player spawn point given.", s);
}

void		mapclean(struct s_mlxvars *s)
{
	int		i;
	char	*newstring;

	i = 0;
	while (i < s->map_height)
	{
		if ((int)ft_strlen(s->map[i]) < s->map_width)
		{
			newstring = malloc(s->map_width + 1);
			ft_strlcpy(newstring, s->map[i], s->map_width + 1);
			free(s->map[i]);
			s->map[i] = newstring;
		}
		i = i + 1;
	}
	mapclean2(s);
}

void		freesplit(int length, struct s_ft_split *split)
{
	while (length >= 0)
	{
		free(split->toreturn[length]);
		length = length - 1;
	}
	free(split->toreturn);
	free(split->str);
	free(split);
}

void		parsespawn(struct s_mlxvars *s, int y)
{
	int i;

	s->player_x = y;
	i = 0;
	while (i < s->map_width)
	{
		if (ft_strchr("NSWE", s->map[y][i]))
		{
			s->player_y = i;
			s->spawn_y = i;
			s->spawn_x = y;
			s->spawndir = s->playerdir;
			return ;
		}
		i = i + 1;
	}
	quit("Error:\nNo player spawn point given.", s);
}

void		parsesettings(struct s_mlxvars *s)
{
	char	*line;

	while (get_next_line(s->mapfile, &line) && s->settingcount < 8)
		if (*line == 'R')
			parseresolution(line, s);
		else if (*line == 'N' || *line == 'W' ||
					*line == 'E' || *line == 'S')
			settexture(line, s);
		else if (*line == 'C')
		{
			if (s->ccolour != 0)
				quit("Error:\nMultiple ceiling colours given", s);
			setfccolour(line, s);
		}
		else if (*line == 'F')
		{
			if (s->fcolour != 0)
				quit("Error\nMultiple floor colours given", s);
			setfccolour(line, s);
		}
	free(line);
	if (s->settingcount != 8)
		quit("Error:\nMap settings incomplete.", s);
}
