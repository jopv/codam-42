/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   handle.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/01/17 14:08:23 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include <stdlib.h>
#include "libft.h"
#include "libftprintf.h"

static int		printcurrentfield(t_field *f, va_list *vars)
{
	if (f->type == 'c')
		printchar(f, vars);
	else if (f->type == 's')
		printstring(f, vars);
	else if (f->type == 'd' || f->type == 'i')
		printint(f, vars);
	else if (f->type == 'u')
		printuint(f, vars);
	else if (f->type == 'x')
		printhexlower(f, vars);
	else if (f->type == 'X')
		printhexupper(f, vars);
	else if (f->type == 'p')
		printpointer(f, vars);
	else if (f->type == '%')
		printpercent(f, vars);
	else if (f->type == 'n')
		*(va_arg(*vars, int*)) = f->totallength + f->length;
	return (f->length);
}

int				handlecurrentfield(char const *fmt, va_list *vars, int tlength)
{
	t_field		*field;
	int			length;

	fmt = fmt + 1;
	field = handleflags(fmt, tlength);
	if (!field)
		return (-1);
	while (*fmt && ft_strchr("-0.*123456789", *fmt))
		fmt = fmt + 1;
	length = 0;
	if (*fmt)
	{
		field->type = *fmt;
		length = printcurrentfield(field, vars);
	}
	if (field->error)
		length = -1;
	free(field);
	return (length);
}
