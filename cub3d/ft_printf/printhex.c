/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   printhex.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/24 17:04:41 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"
#include "libftprintf.h"
#include <stdarg.h>
#include <stdlib.h>

void	printhexlower(t_field *f, va_list *vars)
{
	unsigned int	nbr;
	int				contentlen;
	int				width;
	char			*towrite;

	retrieveuwup(f, vars);
	nbr = va_arg(*vars, unsigned int);
	contentlen = (nbr == 0 && f->useprecision != -2) ? 1 :
					ft_itoa_u_measurelen(nbr, 16) - 1;
	f->length = f->length + contentlen;
	width = printpaddingleft(f, contentlen);
	towrite = ft_itoa_u_base(nbr, "0123456789abcdef", 16);
	if (!towrite || write(1, towrite, contentlen) != contentlen)
		f->error = 1;
	if (towrite)
		free(towrite);
	printpaddingright(f, width);
}

void	printhexupper(t_field *f, va_list *vars)
{
	unsigned int	nbr;
	int				contentlen;
	int				width;
	char			*towrite;

	retrieveuwup(f, vars);
	nbr = va_arg(*vars, unsigned int);
	contentlen = (nbr == 0 && f->useprecision != -2) ? 1 :
					ft_itoa_u_measurelen(nbr, 16) - 1;
	f->length = f->length + contentlen;
	width = printpaddingleft(f, contentlen);
	towrite = ft_itoa_u_base(nbr, "0123456789ABCDEF", 16);
	if (!towrite || write(1, towrite, contentlen) != contentlen)
		f->error = 1;
	if (towrite)
		free(towrite);
	printpaddingright(f, width);
}

void	printpointer(t_field *f, va_list *vars)
{
	unsigned long			nbr;
	int						contentlen;
	int						width;
	char					*towrite;

	retrieveuwup(f, vars);
	nbr = va_arg(*vars, unsigned long);
	contentlen = ft_itoa_u_measurelen(nbr, 16) + 1;
	if (nbr == 0)
		contentlen = (f->useprecision > 2 || f->useprecision == 0) ? 3 : 2;
	f->length = f->length + contentlen;
	width = printpaddingleft(f, contentlen);
	contentlen = contentlen - 2;
	if (nbr)
	{
		towrite = ft_itoa_u_base(nbr, "0123456789abcdef", 16);
		if (!towrite || write(1, towrite, contentlen) != contentlen)
			f->error = 1;
		if (towrite)
			free(towrite);
	}
	else if (contentlen == 1)
		ft_printf_putchar('0', f);
	printpaddingright(f, width);
}
