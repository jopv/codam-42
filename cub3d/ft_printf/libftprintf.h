/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   libftprintf.h                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/01/17 14:16:40 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTPRINTF_H
# define LIBFTPRINTF_H

# include <stdarg.h>

int					ft_printf(char const *fmt, ...);
typedef struct
{
	char			type;
	int				leftjustify;
	int				usewidth;
	char			nbrpadwith;
	int				nbrnegative;
	int				useprecision;
	int				length;
	int				totallength;
	int				error;
}					t_field;
void				printint(t_field *f, va_list *vars);
void				printuint(t_field *f, va_list *vars);
void				printpercent(t_field *f, va_list *vars);
void				printchar(t_field *f, va_list *vars);
void				printstring(t_field *f, va_list *vars);
void				printhexlower(t_field *f, va_list *vars);
void				printhexupper(t_field *f, va_list *vars);
int					printpaddingleft(t_field *f, int conlen);
void				printpaddingright(t_field *f, int width);
void				printpointer(t_field *f, va_list *vars);
int					whetherusewidth(const char *fmt, t_field *f);
int					whethernbruseprecision(const char *fmt, t_field *f);
void				retrieveuwup(t_field *f, va_list *vars);
int					ft_printf_putchar(char c, t_field *f);
t_field				*handleflags(const char *fmt, int tlength);
int					handlecurrentfield(char const *fmt,
						va_list *vars, int tlength);

#endif
