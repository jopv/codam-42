/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   printint.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/24 17:22:41 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"
#include "libftprintf.h"
#include <stdarg.h>
#include <stdlib.h>

void	printint(t_field *f, va_list *vars)
{
	int		nbr;
	int		contentlen;
	int		width;
	char	*towrite;

	retrieveuwup(f, vars);
	nbr = va_arg(*vars, int);
	contentlen = (nbr == 0 && f->useprecision != -2) ? 1
					: ft_itoa_measurelen(nbr, 10) - 1;
	f->nbrnegative = (nbr < 0);
	f->length = f->length + contentlen;
	width = printpaddingleft(f, contentlen);
	if (nbr < 0)
	{
		towrite = ft_itoa_base(nbr, "0123456789", 10) + 1;
		contentlen = contentlen - 1;
	}
	else
		towrite = ft_itoa_base(nbr, "0123456789", 10);
	if (!towrite || write(1, towrite, contentlen) != contentlen)
		f->error = 1;
	if (towrite)
		free(towrite - (nbr < 0));
	printpaddingright(f, width);
}

void	printuint(t_field *f, va_list *vars)
{
	unsigned int	nbr;
	int				contentlen;
	int				width;
	char			*towrite;

	retrieveuwup(f, vars);
	nbr = va_arg(*vars, unsigned int);
	contentlen = (nbr == 0 && f->useprecision != -2) ? 1
					: ft_itoa_u_measurelen(nbr, 10) - 1;
	f->length = f->length + contentlen;
	width = printpaddingleft(f, contentlen);
	towrite = ft_itoa_u_base(nbr, "0123456789", 10);
	if (!towrite || write(1, towrite, contentlen) != contentlen)
		f->error = 1;
	if (towrite)
		free(towrite);
	printpaddingright(f, width);
}

void	printpercent(t_field *f, va_list *vars)
{
	int				width;

	retrieveuwup(f, vars);
	width = printpaddingleft(f, 1);
	ft_printf_putchar('%', f);
	f->length = f->length + 1;
	printpaddingright(f, width);
}
