/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_atoi.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/13 10:46:14 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int			ft_atoi_positnegat(const char *str)
{
	int i;
	int positnegat;

	i = 0;
	positnegat = 0;
	while (!ft_isdigit(str[i]))
	{
		if (positnegat != 0 || !ft_strchr("\t\n\r\v\f-+ ", str[i]))
			return (0);
		else if (str[i] == '-')
			positnegat = -1;
		else if (str[i] == '+')
			positnegat = 1;
		i = i + 1;
	}
	if (positnegat == 0)
		positnegat = 1;
	return (positnegat);
}

int					ft_atoi(const char *str)
{
	int result;
	int positnegat;
	int i;
	int numlen;

	i = 0;
	if (*str == '\0')
		return (0);
	else if (ft_strncmp(str, "-2147483648", 11) == 0)
		return (-2147483648);
	positnegat = ft_atoi_positnegat(str);
	while (!ft_isdigit(str[i]))
		i = i + 1;
	result = 0;
	numlen = 0;
	while (ft_isdigit(str[i]))
	{
		if (numlen > 11)
			return (positnegat == -1 ? 0 : -1);
		result = result * 10 + str[i] - '0';
		i = i + 1;
		numlen = numlen + 1;
	}
	return (result * positnegat);
}
