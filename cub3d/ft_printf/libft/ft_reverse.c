/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_reverse.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/11/27 12:13:29 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_reverse(char *s)
{
	char	*ragf;
	char	*ragb;
	char	tmp;

	ragf = s;
	ragb = s + ft_strlen(s) - 1;
	while (ragb > ragf)
	{
		tmp = *ragf;
		*ragf = *ragb;
		*ragb = tmp;
		ragf = ragf + 1;
		ragb = ragb - 1;
	}
}
