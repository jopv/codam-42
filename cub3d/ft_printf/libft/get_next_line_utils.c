/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   get_next_line_utils.c                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/05/13 14:26:11 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strdupfree(char *src, int srcoffset, int whetherfree)
{
	size_t		src_size;
	char		*dest;

	if (!src)
	{
		dest = malloc(1);
		if (dest == NULL)
			return (NULL);
		ft_memset(dest, 0, 1);
		return (dest);
	}
	src_size = ft_strlen(src + srcoffset) + 1;
	dest = malloc(src_size);
	if (dest == NULL)
		return (NULL);
	ft_strlcpy(dest, src + srcoffset, src_size);
	if (whetherfree)
		free(src);
	return (dest);
}

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	int			i;
	char		*substring;
	size_t		toalloc;
	size_t		slen;

	if (!s)
		return (NULL);
	i = start;
	slen = ft_strlen(s);
	if (start > slen)
		return (ft_strdupfree("", 0, 0));
	else if ((start + len) > slen)
		toalloc = (slen - start) + 1;
	else
		toalloc = len + 1;
	substring = malloc(toalloc);
	if (substring == NULL)
		return (NULL);
	while ((i - start) < len)
	{
		substring[i - start] = s[i];
		i = i + 1;
	}
	substring[i - start] = '\0';
	return (substring);
}

char	*ft_strjoinfree(char *torealloc, char *toappend)
{
	char	*toreturn;
	size_t	len;
	size_t	initiallen;

	if (!torealloc || !toappend)
		return (NULL);
	len = ft_strlen(torealloc) + ft_strlen(toappend) + 1;
	toreturn = malloc(len);
	if (toreturn == NULL)
		return (NULL);
	ft_strlcpy(toreturn, torealloc, len);
	initiallen = 0;
	while (toreturn[initiallen] != '\0' && initiallen < len)
		initiallen = initiallen + 1;
	ft_strlcpy(&toreturn[initiallen], toappend, len - initiallen);
	free(torealloc);
	return (toreturn);
}
