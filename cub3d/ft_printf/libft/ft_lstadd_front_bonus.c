/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lstadd_front_bonus.c                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/12/14 15:45:38 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_front(t_list **firstel, t_list *newel)
{
	if (!firstel)
		return ;
	newel->next = *firstel;
	*firstel = newel;
}
