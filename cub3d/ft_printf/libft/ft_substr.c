/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_substr.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/11/22 12:15:28 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	int			i;
	char		*substring;
	size_t		toalloc;
	size_t		slen;

	if (!s)
		return (NULL);
	i = start;
	slen = ft_strlen(s);
	if (start > slen)
		return (ft_strdup(""));
	else if ((start + len) > slen)
		toalloc = (slen - start) + 1;
	else
		toalloc = len + 1;
	substring = malloc(toalloc);
	if (substring == NULL)
		return (NULL);
	while ((i - start) < len)
	{
		substring[i - start] = s[i];
		i = i + 1;
	}
	substring[i - start] = '\0';
	return (substring);
}
