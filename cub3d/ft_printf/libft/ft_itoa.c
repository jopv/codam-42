/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_itoa.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/11/21 15:58:15 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

int				ft_itoa_measurelen(int nb, int base)
{
	int		len;
	int		measurelen;

	len = 1;
	measurelen = nb;
	if (measurelen < 0)
	{
		measurelen = -measurelen;
		len = len + 1;
	}
	while (measurelen != 0)
	{
		measurelen = measurelen / base;
		len = len + 1;
	}
	return (len);
}

static char		*ft_itoa_makestring(int nb, const char *set, int base)
{
	char	*toreturn;
	int		i;

	toreturn = malloc(ft_itoa_measurelen(nb, base));
	if (toreturn == NULL)
		return (NULL);
	i = 0;
	while (nb >= base)
	{
		toreturn[i] = set[nb % base];
		nb = nb / base;
		i = i + 1;
	}
	toreturn[i] = set[nb % base];
	toreturn[i + 1] = '\0';
	ft_reverse(toreturn);
	return (toreturn);
}

char			*ft_itoa_base(int nb, const char *set, int base)
{
	char	*toreturn;
	char	*numstring;
	int		len;

	if (nb == 0)
		return (ft_strdup("0"));
	else if (nb == -2147483648)
		return (ft_strdup("-2147483648"));
	len = ft_itoa_measurelen(nb, base);
	numstring = ft_itoa_makestring((nb < 0) ? -nb : nb, set, base);
	toreturn = ft_calloc(len, sizeof(char));
	if (nb < 0 && toreturn != NULL)
		*toreturn = '-';
	else if (numstring == NULL || toreturn == NULL)
		return (NULL);
	ft_strlcat(toreturn, numstring, len);
	free(numstring);
	return (toreturn);
}

char			*ft_itoa(int nb)
{
	return (ft_itoa_base(nb, "0123456789", 10));
}
