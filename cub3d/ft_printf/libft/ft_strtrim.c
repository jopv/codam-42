/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strtrim.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/11/22 12:30:38 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static	int		ft_strtrim_goodlen(char const *firstgood, char const *garbage)
{
	int	lastgood;
	int i;

	i = ft_strlen(firstgood) - 1;
	lastgood = i;
	while (i > 0)
	{
		if (!ft_strchr(garbage, firstgood[i]))
		{
			lastgood = i;
			break ;
		}
		i = i - 1;
	}
	return (lastgood + 1);
}

char			*ft_strtrim(char const *s1, char const *garbage)
{
	int		i;
	int		goodlen;

	i = 0;
	if (!s1)
		return (NULL);
	else if (!garbage)
		return (ft_strdup(s1));
	while (s1[i] != '\0')
	{
		if (ft_strchr(garbage, s1[i]))
			i = i + 1;
		else
		{
			goodlen = ft_strtrim_goodlen(&s1[i], garbage);
			return (ft_substr(s1, i, goodlen));
		}
	}
	if (s1[i] == '\0')
		return (ft_strdup(""));
	else
		return (ft_strdup(s1));
}
