/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lstclear_bonus.c                                :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/12/19 14:09:09 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	ft_lstclear(t_list **firstdel, void (*del)(void *))
{
	t_list *current;
	t_list *next;

	current = *firstdel;
	while (current)
	{
		next = current->next;
		del(current->content);
		free(current);
		current = next;
	}
	free(firstdel);
}
