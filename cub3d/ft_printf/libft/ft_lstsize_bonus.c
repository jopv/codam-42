/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_lstsize_bonus.c                                 :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/12/14 15:52:16 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_lstsize(t_list *firstel)
{
	t_list	*currentel;
	int		i;

	currentel = firstel;
	if (!firstel)
		return (0);
	i = 1;
	while (currentel->next)
	{
		currentel = currentel->next;
		i = i + 1;
	}
	return (i);
}
