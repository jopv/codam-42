/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strnstr.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <marvin@codam.nl>                   +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/13 11:55:15 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "libft.h"

char	*ft_strnstr(const char *str, const char *find, size_t len)
{
	size_t i;
	size_t j;
	size_t findlen;

	if (*find == '\0' || (len > 0 && !ft_strncmp(str, find, len)))
		return ((char *)str);
	findlen = ft_strlen(find);
	i = 0;
	while (str[i] != '\0' && i < len)
	{
		j = 0;
		while ((i + j) < len && str[i + j] == find[j])
			j = j + 1;
		if (j == findlen)
			return ((char *)&str[i]);
		i = i + 1;
	}
	return (NULL);
}
