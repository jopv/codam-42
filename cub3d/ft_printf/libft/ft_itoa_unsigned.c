/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_itoa_unsigned.c                                 :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/21 14:12:00 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

int		ft_itoa_u_measurelen(unsigned long nb, unsigned int base)
{
	int					len;
	unsigned long		measurelen;

	len = 1;
	measurelen = nb;
	while (measurelen != 0)
	{
		measurelen = measurelen / base;
		len = len + 1;
	}
	return (len);
}

char	*ft_itoa_u_base(unsigned long nb,
	const char *set, unsigned int base)
{
	char	*toreturn;
	int		i;

	if (nb == 0)
		return (ft_strdup("0"));
	toreturn = malloc(ft_itoa_u_measurelen(nb, base));
	if (toreturn == NULL)
		return (NULL);
	i = 0;
	while (nb >= base)
	{
		toreturn[i] = set[nb % base];
		nb = nb / base;
		i = i + 1;
	}
	toreturn[i] = set[nb % base];
	toreturn[i + 1] = '\0';
	ft_reverse(toreturn);
	return (toreturn);
}
