/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   get_next_line.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/05/13 12:45:42 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"
#include <stdlib.h>
#include <limits.h>

#ifndef BUFFER_SIZE
# define BUFFER_SIZE 1
#endif

struct s_gnl	*newcontent(char *residue, int fd)
{
	char			buffer[BUFFER_SIZE + 1];
	struct s_gnl	*toreturn;

	toreturn = malloc(sizeof(struct s_gnl));
	if (!toreturn)
		return (NULL);
	toreturn->readreturn = 0;
	toreturn->content = ft_strdupfree(residue, 0, 1);
	if (toreturn->content == NULL)
	{
		free(toreturn);
		return (NULL);
	}
	while (1)
	{
		ft_memset(buffer, 0, BUFFER_SIZE + 1);
		toreturn->readreturn = read(fd, buffer, BUFFER_SIZE);
		toreturn->content = ft_strjoinfree(toreturn->content, buffer);
		if (toreturn->readreturn < 1 || ft_strchr(toreturn->content, '\n'))
			break ;
	}
	toreturn->newlineend =
		toreturn->content[ft_strlen(toreturn->content) - 1] == '\n';
	return (toreturn);
}

int				ft_linebreakpos(const char *b, int after)
{
	char	*src;
	int		i;

	src = (char *)b;
	i = 0;
	while (src[i] && src[i] != '\n')
		i = i + 1;
	if (after && src[i])
		i = i + 1;
	return (i);
}

int				get_next_line(int fd, char **line)
{
	static char			*residue[OPEN_MAX];
	struct s_gnl		*s;
	int					readreturn;

	if (fd < 0 || fd >= OPEN_MAX || line == NULL || BUFFER_SIZE <= 0)
		return (-1);
	s = newcontent(residue[fd], fd);
	if (!s)
		return (-1);
	readreturn = s->readreturn;
	*line = ft_substr(s->content, 0, ft_linebreakpos(s->content, 0));
	residue[fd] = ft_strdupfree(s->content, ft_linebreakpos(s->content, 1), 1);
	if (*line == NULL)
		free(residue[fd]);
	else if (readreturn == 0 && (s->newlineend || ft_strlen(residue[fd]) != 0))
		readreturn = 1;
	free(s);
	if (*line == NULL || residue[fd] == NULL)
		return (-1);
	else if (readreturn < 1)
	{
		free(residue[fd]);
		residue[fd] = NULL;
	}
	return (readreturn > 0 ? 1 : readreturn);
}
