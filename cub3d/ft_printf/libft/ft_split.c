/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_split.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <marvin@codam.nl>                   +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/17 10:58:28 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static	int					ft_countsep(char const *str, char c)
{
	char	*rag;
	int		count;
	int		hascontent;

	if (!str)
		return (0);
	rag = (char *)str;
	count = 1;
	hascontent = 0;
	while (*rag != '\0')
	{
		if (*rag == c)
			count = count + 1;
		else
			hascontent = 1;
		rag = rag + 1;
	}
	if (hascontent && *str)
		return (count);
	else
		return (0);
}

static	int					ft_split_add(struct s_ft_split *toadd)
{
	int dealloc;
	int length;

	dealloc = 0;
	if (!toadd->toreturn)
		return (0);
	toadd->toreturn[toadd->words] =
		ft_substr(toadd->str, toadd->start, toadd->index - toadd->start);
	length = ft_strlen(toadd->toreturn[toadd->words]);
	if (length > toadd->longest && length < 512)
		toadd->longest = length;
	if (!toadd->toreturn[toadd->words])
	{
		while (toadd->toreturn[dealloc])
		{
			free(toadd->toreturn[dealloc]);
			dealloc = dealloc + 1;
		}
		free(toadd->toreturn);
		return (0);
	}
	if (*toadd->toreturn[toadd->words])
		toadd->words = toadd->words + 1;
	toadd->start = toadd->index + 1;
	return (1);
}

static struct s_ft_split	*toadd_factory(char *str, char c)
{
	struct s_ft_split *toadd;

	toadd = malloc(sizeof(*toadd));
	toadd->start = 0;
	toadd->words = 0;
	toadd->index = 0;
	toadd->str = str;
	toadd->seperator = c;
	toadd->toreturn = malloc((ft_countsep(str, c) + 1) * sizeof(char *));
	toadd->longest = 0;
	return (toadd);
}

struct s_ft_split			*ft_split_struct(char *str, char c)
{
	struct s_ft_split *toadd;

	toadd = toadd_factory(str, c);
	if (!ft_countsep(str, c))
	{
		toadd->toreturn[0] = NULL;
		return (toadd);
	}
	while (str[toadd->index] != '\0')
	{
		if (str[toadd->index] == c)
			if (!ft_split_add(toadd))
				return (NULL);
		toadd->index = toadd->index + 1;
	}
	if (toadd->words == 0)
	{
		toadd->toreturn[0] = c ? ft_strtrim(str, &c) : ft_strdup(str);
		toadd->words = 1;
	}
	else if (!ft_split_add(toadd))
		return (NULL);
	toadd->toreturn[toadd->words] = NULL;
	return (toadd);
}
