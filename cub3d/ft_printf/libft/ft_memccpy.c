/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_memccpy.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/10/30 11:06:23 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

void	*ft_memccpy(void *dst, const void *src, int c, size_t len)
{
	size_t				i;
	unsigned char		*tempdest;
	unsigned const char	*tempsrc;

	tempdest = (unsigned char *)dst;
	tempsrc = (unsigned const char *)src;
	i = 0;
	while (i < len)
	{
		tempdest[i] = tempsrc[i];
		if (tempsrc[i] == (unsigned char)c)
		{
			tempdest[i] = tempsrc[i];
			return ((void *)&tempdest[i + 1]);
		}
		i = i + 1;
	}
	return (NULL);
}
