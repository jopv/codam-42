/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_memcpy.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/10/30 11:03:03 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

void	*ft_memcpy(void *dst, const void *src, size_t len)
{
	size_t			i;
	unsigned char	*tempdest;
	unsigned char	*tempsrc;

	if (dst == NULL && src == NULL)
		return (dst);
	tempdest = (unsigned char *)dst;
	tempsrc = (unsigned char *)src;
	i = 0;
	while (i < len)
	{
		tempdest[i] = tempsrc[i];
		i = i + 1;
	}
	return (dst);
}
