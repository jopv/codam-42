/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   flags.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/25 12:08:35 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"
#include "libft.h"
#include <stdlib.h>

int				whethernbruseprecision(const char *fmt, t_field *f)
{
	int move;

	move = 0;
	if (f->useprecision != 0)
		return (0);
	if (*fmt == '.')
	{
		if (*(fmt + 1) != '*')
		{
			f->useprecision = ft_atoi(fmt + 1);
			f->nbrpadwith = ' ';
			if (f->useprecision < 1)
			{
				f->useprecision = -2;
				return (1);
			}
			move = (*(fmt + 1) == '-') ? 2 : 1;
			while (*(fmt + move) && ft_isdigit(*(fmt + move)))
				move = move + 1;
		}
		else
			f->useprecision = -1;
	}
	return (move ? (move - 1) : 0);
}

int				whetherusewidth(const char *fmt, t_field *f)
{
	int	move;

	move = 0;
	if (f->usewidth != 0)
		return (0);
	if (*fmt == '*' && *(fmt - 1) != '.')
		f->usewidth = -1;
	else if (ft_isdigit(*fmt))
	{
		f->usewidth = ft_atoi(fmt);
		while (*(fmt + move) && ft_isdigit(*(fmt + move)))
			move = move + 1;
	}
	return (move);
}

void			retrieveuwup(t_field *f, va_list *vars)
{
	if (f->usewidth == -1)
	{
		f->usewidth = va_arg(*vars, int);
		if (f->usewidth < 0)
		{
			f->usewidth = -f->usewidth;
			f->leftjustify = 1;
		}
	}
	if (f->useprecision == -1)
	{
		f->useprecision = va_arg(*vars, int);
		if (f->useprecision == 0)
			f->useprecision = -2;
	}
}

static t_field	*t_field_factory(int tlength)
{
	t_field *toreturn;

	toreturn = malloc(sizeof(*toreturn));
	if (!toreturn)
		return (NULL);
	toreturn->type = 0;
	toreturn->leftjustify = 0;
	toreturn->usewidth = 0;
	toreturn->nbrpadwith = ' ';
	toreturn->useprecision = 0;
	toreturn->length = 0;
	toreturn->totallength = tlength;
	toreturn->nbrnegative = 0;
	toreturn->error = 0;
	return (toreturn);
}

t_field			*handleflags(const char *fmt, int tlength)
{
	t_field *toreturn;

	toreturn = t_field_factory(tlength);
	if (!toreturn)
		return (NULL);
	while (*fmt && ft_strchr("-0.*123456789", *fmt))
	{
		if (*fmt == '0' && toreturn->usewidth == 0)
		{
			toreturn->nbrpadwith = '0';
			fmt = fmt + 1;
			continue ;
		}
		if (*fmt == '-')
		{
			toreturn->leftjustify = 1;
			fmt = fmt + 1;
			continue ;
		}
		fmt = fmt + whetherusewidth(fmt, toreturn);
		fmt = fmt + whethernbruseprecision(fmt, toreturn) + 1;
	}
	return (toreturn);
}
