/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_printf.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/01/16 12:45:15 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include "libftprintf.h"
#include "libft.h"

static int		fmtmove(char const *fmt)
{
	int move;

	move = 1;
	while (*(fmt + move) && ft_strchr("-0.*123456789", *(fmt + move)))
		move = move + 1;
	return (move);
}

static int		myprintf(char const *fmt, va_list *vars)
{
	int length;
	int fieldlength;

	length = 0;
	while (*fmt)
	{
		if (*fmt != '%')
		{
			if (ft_printf_putchar(*fmt, NULL))
				return (-1);
			length = length + 1;
		}
		else
		{
			fieldlength = handlecurrentfield(fmt, vars, length);
			if (fieldlength == -1)
				return (-1);
			length = length + fieldlength;
			fmt = fmt + fmtmove(fmt);
			if (!*fmt)
				break ;
		}
		fmt = fmt + 1;
	}
	return (length);
}

int				ft_printf(char const *fmt, ...)
{
	int		toreturn;
	va_list	variables;

	va_start(variables, fmt);
	toreturn = myprintf(fmt, &variables);
	va_end(variables);
	return (toreturn);
}
