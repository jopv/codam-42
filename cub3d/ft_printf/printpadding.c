/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   printpadding.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/24 17:02:05 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "libftprintf.h"
#include <stdarg.h>
#include <unistd.h>

static void	ptrnegprefix(t_field *f)
{
	if (f->nbrpadwith == ' ')
	{
		if (f->nbrnegative)
			ft_printf_putchar('-', f);
		if (f->type == 'p')
			if (write(1, "0x", 2) != 2)
				f->error = 1;
	}
}

static int	precisionprepare(t_field *f)
{
	if (ft_strchr("diuxX", f->type) && f->useprecision > 0)
		f->nbrpadwith = ' ';
	return ((f->type == 'p') ? 2 : f->nbrnegative);
}

int			printpaddingleft(t_field *f, int conlen)
{
	int width;
	int precision;
	int compensate;

	compensate = precisionprepare(f);
	precision = f->useprecision - conlen + compensate;
	width = (precision > 0) ? (f->usewidth - f->useprecision - compensate)
			: (f->usewidth - conlen);
	if (f->nbrnegative && f->nbrpadwith == '0')
		ft_printf_putchar('-', f);
	if (!f->leftjustify)
		while (width > 0)
		{
			ft_printf_putchar(f->nbrpadwith, f);
			f->length = f->length + 1;
			width = width - 1;
		}
	ptrnegprefix(f);
	while (precision > 0)
	{
		ft_printf_putchar('0', f);
		f->length = f->length + 1;
		precision = precision - 1;
	}
	return (width);
}

void		printpaddingright(t_field *f, int width)
{
	while (width > 0)
	{
		ft_printf_putchar(' ', f);
		f->length = f->length + 1;
		width = width - 1;
	}
}
