/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   printtext.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/02/24 17:18:09 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"
#include "libftprintf.h"
#include <stdarg.h>

void		printchar(t_field *f, va_list *vars)
{
	int width;

	retrieveuwup(f, vars);
	width = printpaddingleft(f, 1);
	ft_printf_putchar(va_arg(*vars, int), f);
	f->length = f->length + 1;
	printpaddingright(f, width);
}

void		printstring(t_field *f, va_list *vars)
{
	char	*s;
	int		contentlen;
	int		width;

	retrieveuwup(f, vars);
	s = va_arg(*vars, char *);
	if (!s)
		s = "(null)";
	contentlen = ft_strlen(s);
	if (f->useprecision > 0)
	{
		if (f->useprecision < contentlen)
			contentlen = f->useprecision;
		f->useprecision = 0;
	}
	if (f->useprecision == -2)
		contentlen = 0;
	f->length = f->length + contentlen;
	width = printpaddingleft(f, contentlen);
	if (write(1, s, contentlen) != contentlen)
		f->error = 1;
	printpaddingright(f, width);
}

int			ft_printf_putchar(char c, t_field *f)
{
	int wr;

	wr = (write(1, &c, 1) != 1);
	if (wr && f)
		f->error = 1;
	return (wr);
}
