/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   arguments.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/09/16 13:25:39 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include "libftprintf.h"
#include "mlx.h"
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

void	freeimage(t_image *tofree, void *mlx)
{
	if (!tofree)
		return ;
	mlx_destroy_image(mlx, tofree->img);
	free(tofree);
}

void	quit(char *message, struct s_mlxvars *s)
{
	int			i;

	if (s->window)
		mlx_destroy_window(s->mlx, s->window);
	freeimage(s->westtex, s->mlx);
	freeimage(s->easttex, s->mlx);
	freeimage(s->southtex, s->mlx);
	freeimage(s->northtex, s->mlx);
	freeimage(s->sprite1tex, s->mlx);
	freeimage(s->sprite2tex, s->mlx);
	freeimage(s->fb1, s->mlx);
	i = 0;
	if (s->map)
		while (i < s->map_height)
		{
			if (s->map[i])
				free(s->map[i]);
			i = i + 1;
		}
	free(s->map);
	close(s->mapfile);
	free(s->mlx);
	free(s);
	ft_printf("%s\n", message);
	exit(0);
}

int		quitrequested(struct s_mlxvars *s)
{
	quit("Afgesloten / Quitted", s);
	return (0);
}

void	handlearguments(struct s_mlxvars *s, int argc, char **argv)
{
	if (argc < 2 || argc > 3 || (argc == 3 && ft_strncmp(argv[2], "--save", 6)))
		quit("Error:\nInvalid command. Usage: ./cub3d [map file] [--save]", s);
	else if (argc == 3 && !ft_strncmp(argv[2], "--save", 6))
		s->imgfile = open("spawn.bmp", O_WRONLY | O_CREAT | O_TRUNC, 0644);
	s->mapfile = open(argv[1], O_RDONLY);
	if (!ft_strnstr(argv[1], ".cub", ft_strlen(argv[1])))
		quit("Error:\nMap file must be in .cub format.", s);
	if (s->mapfile < 0 || read(s->mapfile, NULL, 0) < 0)
		quit("Error:\nMap file is invalid, nonexistent, or failed to read.", s);
	parse(s);
}

t_image	**textureforface(int face, struct s_mlxvars *s)
{
	if (face == NORTH)
		return (&s->northtex);
	else if (face == SOUTH)
		return (&s->southtex);
	else if (face == WEST)
		return (&s->westtex);
	else if (face == EAST)
		return (&s->easttex);
	else if (face == SPRITE1)
		return (&s->sprite1tex);
	else if (face == SPRITE2)
		return (&s->sprite2tex);
	return (0);
}
