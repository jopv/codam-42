/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   parse1.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/09/11 15:05:20 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include "mlx.h"
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include "linkedlist.h"

void			parseresolution(char *line, struct s_mlxvars *s)
{
	int					length;
	struct s_ft_split	*split;
	int					max_x;
	int					max_y;

	split = ft_split_struct(line, ' ');
	length = 0;
	while (split != NULL && split->toreturn[length] != NULL)
		length = length + 1;
	if (length != 3 || s->res_x != 0 || s->res_y != 0)
		quit("Error:\nMap file has invalid resolution input.", s);
	s->res_x = ft_atoi(split->toreturn[1]);
	s->res_y = ft_atoi(split->toreturn[2]);
	if (s->res_x < 1 || s->res_y < 1)
		quit("Error:\nResolution is zero.", s);
	mlx_get_screen_size(s->mlx, &max_x, &max_y);
	if (s->res_x > max_x)
		s->res_x = max_x;
	if (s->res_y > max_y)
		s->res_y = max_y;
	s->settingcount = s->settingcount + 1;
	freesplit(length, split);
}

void			settexture(char *line, struct s_mlxvars *s)
{
	int						length;
	struct s_ft_split		*split;

	split = ft_split_struct(line, ' ');
	length = 0;
	while (split != NULL && split->toreturn[length] != NULL)
		length = length + 1;
	if (length != 2)
		quit("Error:\nMap file has invalid texture path.", s);
	else if (ft_strncmp(split->toreturn[0], "NO", 2) == 0)
		loadtexture(split->toreturn[1], NORTH, s);
	else if (ft_strncmp(split->toreturn[0], "SO", 2) == 0)
		loadtexture(split->toreturn[1], SOUTH, s);
	else if (ft_strncmp(split->toreturn[0], "WE", 2) == 0)
		loadtexture(split->toreturn[1], WEST, s);
	else if (ft_strncmp(split->toreturn[0], "EA", 2) == 0)
		loadtexture(split->toreturn[1], EAST, s);
	else if (*split->toreturn[0] == 'S')
		loadtexture(split->toreturn[1], SPRITE1, s);
	s->settingcount = s->settingcount + 1;
	freesplit(length, split);
}

void			setfccolour(char *line, struct s_mlxvars *s)
{
	int					r;
	int					g;
	int					b;
	int					length;
	struct s_ft_split	*split;

	split = ft_split_struct(line, ',');
	length = 0;
	while (split != NULL && split->toreturn[length] != NULL)
		length = length + 1;
	if (length != 3 || ft_strchr(line, '-'))
		quit("Error:\nMap file has invalid colour.", s);
	r = ft_atoi(split->toreturn[0] + 2);
	g = ft_atoi(split->toreturn[1]);
	b = ft_atoi(split->toreturn[2]);
	if (r > 255 || g > 255 || b > 255)
		quit("Error:\nMap file has invalid colour.", s);
	else if (*split->toreturn[0] == 'F')
		s->fcolour = rgbt(r, g, b, 0);
	else if (*split->toreturn[0] == 'C')
		s->ccolour = rgbt(r, g, b, 0);
	s->settingcount = s->settingcount + 1;
	freesplit(length, split);
}

t_listitem		*readmap(struct s_mlxvars *s)
{
	int			spawns;
	char		*line;
	t_listitem	*maprows;

	spawns = 0;
	maprows = NULL;
	while (get_next_line(s->mapfile, &line))
	{
		if (line == NULL)
			quit("Error:\nCouldn't allocate memory for parsing.", s);
		if (ft_strrchr(line, 'N') || ft_strrchr(line, 'E') ||
			ft_strrchr(line, 'S') || ft_strrchr(line, 'W'))
			spawns = spawns + 1;
		if (spawns > 1)
			quit("Error:\nMultiple spawn points in map.", s);
		maprows = additem(maprows, line);
		s->map_height = s->map_height + 1;
		if (ft_strlen(line) > (size_t)s->map_width)
			s->map_width = ft_strlen(line);
		free(line);
	}
	free(line);
	if (spawns == 0)
		quit("Error:\nNo player spawn point given.", s);
	return (maprows);
}

void			parse(struct s_mlxvars *s)
{
	t_listitem			*maprows;
	t_listitem			*maprow;
	int					i;

	parsesettings(s);
	maprows = readmap(s);
	s->map = malloc(sizeof(char *) * s->map_height);
	maprow = maprows;
	i = 0;
	while (i < s->map_height)
	{
		s->map[i] = maprow->content;
		maprow = maprow->next;
		i = i + 1;
	}
	freelist(maprows);
	parseplayerdir(s);
	mapclean(s);
}
