/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   giveframe.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/01 15:37:49 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include "mlx.h"

void	drawhealthbar(struct s_mlxvars *s)
{
	unsigned int	greenpix;
	unsigned int	redpix;
	int				i;

	greenpix = s->health * s->res_y;
	redpix = s->res_y - greenpix;
	i = 0;
	while (i < DATA_HPWIDTH)
	{
		s->fb1->brush.mlxformat = DATA_HPGREEN;
		drawverticalline(s->fb1, s->res_x - i, 0, greenpix);
		s->fb1->brush.mlxformat = DATA_HPRED;
		drawverticalline(s->fb1, s->res_x - i, greenpix, redpix);
		i = i + 1;
	}
	s->fb1->brush.mlxformat = 0x00000000;
	drawverticalline(s->fb1, s->res_x - i, 0, s->res_y);
	drawverticalline(s->fb1, s->res_x - i - 1, 0, s->res_y);
}

int		rendernextframe(struct s_mlxvars *s)
{
	mlx_sync(MLX_SYNC_IMAGE_WRITABLE, s->fb1->img);
	if (s->health > 0 && s->walkforward)
		walkforward(s);
	if (s->health > 0 && s->walkbackward)
		walkbackward(s);
	if (s->health > 0 && s->walkleft)
		walkleft(s);
	if (s->health > 0 && s->walkright)
		walkright(s);
	if (s->viewmoved ||
		s->walkleft || s->walkright || s->walkforward || s->walkbackward)
	{
		s->renderprecision = DATA_PRECISION;
		doraycast(s);
		drawhealthbar(s);
		mlx_put_image_to_window(s->mlx, s->window, s->fb1->img, 0, 0);
		mlx_sync(MLX_SYNC_WIN_FLUSH_CMD, s->window);
		s->viewmoved = 0;
	}
	return (1);
}
