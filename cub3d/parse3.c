/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   parse3.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/24 17:24:14 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include "linkedlist.h"

void	mapclean2(struct s_mlxvars *s)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (i < s->map_height)
	{
		while (j < s->map_width)
		{
			if (!ft_strchr("1234", s->map[i][j]))
				s->map[i][j] = '0';
			j = j + 1;
		}
		i = i + 1;
		j = 0;
	}
}

void	parseplayerdir(struct s_mlxvars *s)
{
	int i;

	i = 0;
	while (i < s->map_height)
	{
		if (ft_strchr(s->map[i], 'N'))
			s->playerdir = NORTH;
		else if (ft_strchr(s->map[i], 'E'))
			s->playerdir = EAST;
		else if (ft_strchr(s->map[i], 'S'))
			s->playerdir = SOUTH;
		else if (ft_strchr(s->map[i], 'W'))
			s->playerdir = WEST;
		else
		{
			i = i + 1;
			continue ;
		}
		break ;
	}
	parsespawn(s, i);
}
