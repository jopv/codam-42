/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   collision.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/15 18:35:10 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void		bumpsprite(struct s_mlxvars *s, double oldx, double oldy)
{
	if (s->map[(int)s->player_x][(int)s->player_y] == '2')
	{
		s->health = s->health - DATA_DAMAGE;
		if (s->health < 0)
		{
			s->health = 0;
			s->walkforward = 0;
			s->walkbackward = 0;
			s->walkleft = 0;
			s->walkright = 0;
			s->viewmoved = 1;
		}
	}
	else if (s->map[(int)s->player_x][(int)s->player_y] == '3')
		s->health = 1;
	s->player_x = oldx - (s->player_x - oldx) * DATA_KNOCKBACK;
	s->player_y = oldy - (s->player_y - oldy) * DATA_KNOCKBACK;
	if (s->map[(int)s->player_x][(int)s->player_y] == '1' ||
		voidtile(s->player_x, s->player_y, s))
	{
		s->player_x = oldx;
		s->player_y = oldy;
	}
}

int				voidtile(int pos1, int pos2, struct s_mlxvars *s)
{
	return (pos1 < 0 || pos1 + 1 > s->map_height
			|| pos2 < 0 || pos2 + 1 > s->map_width);
}

static int		bumpvoid(struct s_mlxvars *s, double oldx, double oldy)
{
	if (voidtile(s->player_x, s->player_y, s))
	{
		s->player_x = oldx;
		s->player_y = oldy;
		s->health = 0;
		return (1);
	}
	return (0);
}

void			bumpwall(struct s_mlxvars *s, double oldx, double oldy)
{
	bumpvoid(s, oldx, oldy);
	if (s->map[(int)oldx][(int)oldy] == '1')
		return ;
	if (s->map[(int)s->player_x][(int)s->player_y] == '1' &&
			s->map[(int)s->player_x][(int)oldy] != '1')
		s->player_y = oldy;
	else if (s->map[(int)s->player_x][(int)s->player_y] == '1' &&
			s->map[(int)oldx][(int)s->player_y] != '1')
		s->player_x = oldx;
	else if (s->map[(int)s->player_x][(int)s->player_y] == '1')
	{
		s->player_x = oldx;
		s->player_y = oldy;
	}
	else if (s->map[(int)s->player_x][(int)s->player_y] == '2'
		|| s->map[(int)s->player_x][(int)s->player_y] == '3')
		bumpsprite(s, oldx, oldy);
}
