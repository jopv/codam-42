/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   walk.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/10 14:07:19 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include <math.h>

void	walkforward(struct s_mlxvars *s)
{
	double	oldx;
	double	oldy;

	oldx = s->player_x;
	oldy = s->player_y;
	s->player_x = s->player_x + cos(degtorad(s->playerdir)) * DATA_MOVESPEED;
	s->player_y = s->player_y + sin(degtorad(s->playerdir)) * DATA_MOVESPEED;
	bumpwall(s, oldx, oldy);
}

void	walkbackward(struct s_mlxvars *s)
{
	double	oldx;
	double	oldy;

	oldx = s->player_x;
	oldy = s->player_y;
	s->player_x = s->player_x - cos(degtorad(s->playerdir)) * DATA_MOVESPEED;
	s->player_y = s->player_y - sin(degtorad(s->playerdir)) * DATA_MOVESPEED;
	bumpwall(s, oldx, oldy);
}

void	walkleft(struct s_mlxvars *s)
{
	double	oldx;
	double	oldy;

	oldx = s->player_x;
	oldy = s->player_y;
	s->player_x = s->player_x +
		cos(degtorad(s->playerdir + 90)) * DATA_MOVESPEED;
	s->player_y = s->player_y +
		sin(degtorad(s->playerdir + 90)) * DATA_MOVESPEED;
	bumpwall(s, oldx, oldy);
}

void	walkright(struct s_mlxvars *s)
{
	double	oldx;
	double	oldy;

	oldx = s->player_x;
	oldy = s->player_y;
	s->player_x = s->player_x +
		cos(degtorad(s->playerdir - 90)) * DATA_MOVESPEED;
	s->player_y = s->player_y +
		sin(degtorad(s->playerdir - 90)) * DATA_MOVESPEED;
	bumpwall(s, oldx, oldy);
}
