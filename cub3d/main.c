/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   main.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/09/14 14:38:29 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "mlx.h"
#include "cub3d.h"
#include <stdlib.h>
#include "libftprintf.h"

static void			debugmlxvars(struct s_mlxvars *f)
{
	int i;

	if (!DEBUGLOG)
		return ;
	ft_printf("Horizontal resolution: %i\n", f->res_x);
	ft_printf("Vertical resolution: %i\n", f->res_y);
	ft_printf("Map width: %i\nMap height: %i\n", f->map_width, f->map_height);
	ft_printf("Spawn point: %i,%i\n", f->spawn_y, f->spawn_x);
	i = 0;
	while (i < f->map_height)
	{
		ft_printf("Rij/Row %i:\t%s\n", i, f->map[i]);
		i = i + 1;
	}
}

static void			setstufftozero(struct s_mlxvars *s)
{
	s->window = 0;
	s->fb1 = 0;
	s->imgfile = 0;
	s->mapfile = 0;
	s->northtex = 0;
	s->southtex = 0;
	s->westtex = 0;
	s->easttex = 0;
	s->sprite1tex = 0;
	s->sprite2tex = 0;
	s->res_x = 0;
	s->res_y = 0;
	s->spritecast = 0;
	s->settingcount = 0;
	s->fcolour = 0;
	s->ccolour = 0;
	s->map_width = 0;
	s->map_height = 0;
	s->map = 0;
	s->playerdir = 0;
	s->walkforward = 0;
	s->walkbackward = 0;
	s->walkleft = 0;
	s->walkright = 0;
}

struct s_mlxvars	*mlxvarsfactory(void)
{
	struct s_mlxvars *s;

	s = malloc(sizeof(struct s_mlxvars));
	setstufftozero(s);
	s->mlx = mlx_init();
	s->renderprecision = DATA_PRECISION;
	s->columnsleft = s->renderprecision;
	s->health = 1;
	s->viewmoved = 1;
	loadtexture(DATA_SPRITE2_PATH, SPRITE2, s);
	return (s);
}

int					main(int argc, char **argv)
{
	struct s_mlxvars *s;

	s = mlxvarsfactory();
	handlearguments(s, argc, argv);
	debugmlxvars(s);
	s->fb1 = imagefactory(s, s->res_x, s->res_y);
	if (s->imgfile == 0)
	{
		s->window = mlx_new_window(s->mlx, s->res_x, s->res_y, argv[1]);
		mlx_hook(s->window, KEY_PRESSED, 1L << 0, buttonpress, s);
		mlx_hook(s->window, KEY_RELEASED, 1L << 1, buttonrelease, s);
		mlx_hook(s->window, QUIT_REQUESTED, 0L, quitrequested, s);
		mlx_hook(s->window, CURSOR_MOVED, 1L << 6, mousemove, s);
		mlx_loop_hook(s->mlx, rendernextframe, s);
		mlx_loop(s->mlx);
	}
	else
	{
		doraycast(s);
		drawhealthbar(s);
		doscreenshot(s);
	}
	return (0);
}
