/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   math.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/07 11:50:51 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "cub3d.h"

unsigned int	rgbt(unsigned char r, unsigned char g, unsigned char b,
						unsigned char t)
{
	return (t << 24 | r << 16 | g << 8 | b);
}

double			degtorad(double deg)
{
	return ((deg * M_PI) / 180);
}

double			pt(double x, double y)
{
	return (sqrt(pow(x, 2) + pow(y, 2)));
}

int				fixangle(int deg)
{
	while (deg < 0)
		deg = deg + 360;
	while (deg > 360)
		deg = deg - 360;
	return (deg);
}

int				wallfacing(int side, int castangle)
{
	int angle;

	angle = fixangle(castangle);
	if (angle < EAST)
		return (side ? EAST : SOUTH);
	else if (angle < NORTH)
		return (side ? EAST : NORTH);
	else if (angle < WEST)
		return (side ? WEST : NORTH);
	else
		return (side ? WEST : SOUTH);
}
