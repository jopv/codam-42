/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   screenshot.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/22 13:17:59 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include <unistd.h>

void	mspadding(int amount, int file)
{
	char padding;

	padding = '\x00';
	while (amount > 0)
	{
		write(file, &padding, 1);
		amount = amount - 1;
	}
}

void	msheader(unsigned int width, unsigned int height, int file)
{
	unsigned int	size;

	size = (3 * (width * height)) +
		((width * 3) % 4) ? (height * (4 - ((width * 3) % 4))) : 0;
	write(file, "\x42\x4d", 2);
	write(file, &size, 4);
	mspadding(4, file);
	write(file, "\x36\x00\x00\x00\x28\x00\x00\x00", 8);
	write(file, &width, 4);
	write(file, &height, 4);
	write(file, "\x01\x00\x18", 3);
	mspadding(25, file);
}

void	doscreenshot(struct s_mlxvars *s)
{
	int				x;
	int				y;
	union u_colour	colour;

	msheader(s->res_x, s->res_y, s->imgfile);
	y = s->res_y - 1;
	while (y >= 0)
	{
		x = 0;
		while (x < s->res_x)
		{
			colour.mlxformat = eyedropper(s->fb1, x, y);
			write(s->imgfile, &colour.rgbt[COLOUR_B], 1);
			write(s->imgfile, &colour.rgbt[COLOUR_G], 1);
			write(s->imgfile, &colour.rgbt[COLOUR_R], 1);
			x = x + 1;
		}
		if ((s->res_x * 3) % 4 != 0)
			mspadding(4 - ((s->res_x * 3) % 4), s->imgfile);
		y = y - 1;
	}
	close(s->imgfile);
	quit("Screenshot in spawn.bmp.", s);
}
