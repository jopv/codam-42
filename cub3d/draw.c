/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   draw.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/20 16:19:18 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:34 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include <math.h>

static int			drawtexturefb1(int x, t_ray *r, struct s_mlxvars *s)
{
	int		pixelpos;
	int		pixelsize;
	int		texcol;
	t_image	*texture;
	int		i;

	texture = *(textureforface(r->facing, s));
	pixelpos = (s->res_y / 2) - r->wallheight;
	if (r->spritenow && r->sprite_x != -1 && r->sprite_y != -1)
		texcol = s->spritecast;
	else
		texcol = ((int)(texture->x * (r->pos_x + r->pos_y)) % texture->x);
	i = 0;
	while (i < (r->wallheight * 2))
	{
		pixelsize = (r->wallheight * 2 + i) / texture->y;
		s->fb1->brush.mlxformat = eyedropper(texture, texcol, i);
		if (s->fb1->brush.rgbt[COLOUR_T] < 255)
			drawverticalline(s->fb1, x, pixelpos, pixelsize);
		pixelpos = pixelpos + pixelsize;
		i = i + 1;
		if (i == texture->y || pixelpos > s->res_y)
			break ;
	}
	return (texcol);
}

static int			drawsprite(int x, t_ray *r, struct s_mlxvars *s)
{
	t_image		*texture;
	double		distance;

	if (r->sprite_x == -1 || r->sprite_y == -1)
	{
		s->spritecast = 1;
		return (0);
	}
	distance = pt(s->player_x - r->sprite_x, s->player_y - r->sprite_y);
	r->facing = (s->map[r->sprite_x][r->sprite_y] == '2') ? SPRITE1 : SPRITE2;
	r->spritenow = 1;
	texture = *(textureforface(r->facing, s));
	r->wallheight = (int)((s->res_y / 2) / distance) > texture->y / 2 ?
		(int)((s->res_y / 2) / distance) : texture->y / 2;
	if (s->spritecast > texture->x)
	{
		r->spritecount = r->spritecount - 1;
		if (r->spritecount < 1)
			return (1);
		else
			s->spritecast = 1;
	}
	drawtexturefb1(x, r, s);
	s->spritecast = s->spritecast + (distance / (s->res_x * DATA_SPRITEWIDTH));
	return (1);
}

static void			drawstars(t_image *image, int x, int castangle)
{
	image->brush.mlxformat = DATA_STAR;
	if (castangle % 23 == 0)
		drawverticalline(image, x, 100, 10);
	else if (castangle % 16 == 0)
		drawverticalline(image, x, 70, 10);
	else if (castangle % 37 == 0)
		drawverticalline(image, x, 30, 10);
	else if (castangle % 29 == 0)
		drawverticalline(image, x, 140, 10);
	else if (castangle % 43 == 0)
		drawverticalline(image, x, 200, 10);
	else if (castangle % 49 == 0)
		drawverticalline(image, x, 230, 10);
}

void				setbrush(t_image *image, unsigned int colour, double health)
{
	union u_colour c;

	c.mlxformat = colour;
	if (health == 0)
		c.rgbt[COLOUR_R] = c.rgbt[COLOUR_R] + 200;
	image->brush = c;
}

int					drawcwf(int x, t_ray *r, int castangle, struct s_mlxvars *s)
{
	setbrush(s->fb1, s->ccolour, s->health);
	drawverticalline(s->fb1, x, 0, (s->res_y / 2) - r->wallheight);
	drawstars(s->fb1, x, castangle);
	r->facing = wallfacing(r->side, castangle);
	setbrush(s->fb1, s->fcolour, s->health);
	drawverticalline(s->fb1, x, (s->res_y / 2) - r->wallheight,
		r->wallheight * 2);
	drawtexturefb1(x, r, s);
	setbrush(s->fb1, s->fcolour, s->health);
	drawverticalline(s->fb1, x, (s->res_y / 2) + r->wallheight,
		(s->res_y / 2) - r->wallheight);
	if (drawsprite(x, r, s))
	{
		s->columnsleft = 0;
		s->renderprecision = 1;
	}
	else
	{
		s->renderprecision = DATA_PRECISION;
		s->columnsleft--;
	}
	(s->columnsleft > 0) ? drawcwf(x + 1, r, castangle, s)
							: (s->columnsleft = s->renderprecision);
	return (0);
}
