/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   button.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/02 17:09:57 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include "mlx.h"
#include <stdlib.h>

static void	respawn(struct s_mlxvars *s)
{
	if (s->health < DATA_HEAL)
		s->health = DATA_HEAL;
	s->playerdir = s->spawndir;
	s->player_x = s->spawn_x;
	s->player_y = s->spawn_y;
	s->viewmoved = 1;
}

int			buttonpress(int button, struct s_mlxvars *s)
{
	if (s->health == 0)
		return (0);
	if (button == BTN_RIGHT || button == BTN_NUM_6
		|| button == BTN_NUM_3 || button == BTN_NUM_9)
		s->walkright = 1;
	if (button == BTN_LEFT || button == BTN_NUM_4
		|| button == BTN_NUM_1 || button == BTN_NUM_7)
		s->walkleft = 1;
	if (button == BTN_FORWARD || button == BTN_FORWARD_ALT
		|| button == BTN_NUM_8 || button == BTN_NUM_7 || button == BTN_NUM_9)
		s->walkforward = 1;
	if (button == BTN_BACKWARD || button == BTN_BACKWARD_ALT
		|| button == BTN_NUM_2 || button == BTN_NUM_5
		|| button == BTN_NUM_3 || button == BTN_NUM_1)
		s->walkbackward = 1;
	if (button == BTN_LEFT_ALT)
		s->playerdir = s->playerdir + DATA_LOOKSPEED * 2;
	if (button == BTN_RIGHT_ALT)
		s->playerdir = s->playerdir - DATA_LOOKSPEED * 2;
	if (button == BTN_LEFT_ALT || button == BTN_RIGHT_ALT)
		s->viewmoved = 1;
	return (0);
}

int			buttonrelease(int button, struct s_mlxvars *s)
{
	if (button == BTN_HEAL || button == BTN_HEAL_NUM)
		respawn(s);
	if (button == BTN_ESC)
		quit("Afgesloten / Quitted", s);
	if (button == BTN_RIGHT || button == BTN_NUM_6
		|| button == BTN_NUM_3 || button == BTN_NUM_9)
		s->walkright = 0;
	if (button == BTN_LEFT || button == BTN_NUM_4
		|| button == BTN_NUM_1 || button == BTN_NUM_7)
		s->walkleft = 0;
	if (button == BTN_FORWARD || button == BTN_FORWARD_ALT
		|| button == BTN_NUM_8 || button == BTN_NUM_7 || button == BTN_NUM_9)
		s->walkforward = 0;
	if (button == BTN_BACKWARD || button == BTN_BACKWARD_ALT
		|| button == BTN_NUM_2 || button == BTN_NUM_5
		|| button == BTN_NUM_3 || button == BTN_NUM_1)
		s->walkbackward = 0;
	return (0);
}

int			mousemove(int x, int y, struct s_mlxvars *s)
{
	s->viewmoved = 1;
	if (s->health == 0)
	{
		mlx_mouse_show();
		return (y);
	}
	mlx_mouse_hide();
	if (x < s->res_x / 2)
		s->playerdir = s->playerdir + DATA_LOOKSPEED;
	else if (x > s->res_x / 2)
		s->playerdir = s->playerdir - DATA_LOOKSPEED;
	mlx_mouse_move(s->window, s->res_x / 2, s->res_y / 2);
	return (y);
}
