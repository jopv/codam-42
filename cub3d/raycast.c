/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   raycast.c                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/10/14 17:54:17 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include <math.h>
#include <stdlib.h>

static void		throwray(t_ray *r, struct s_mlxvars *s)
{
	while (1)
	{
		r->pos_x = r->pos_x + r->path_cos;
		r->side = 0;
		if (voidtile(r->pos_x, r->pos_y, s) ||
			ft_strchr("14", s->map[(int)r->pos_x][(int)r->pos_y]))
			break ;
		r->pos_y = r->pos_y + r->path_sin;
		r->side = 1;
		if (voidtile(r->pos_x, r->pos_y, s) ||
			ft_strchr("14", s->map[(int)r->pos_x][(int)r->pos_y]))
			break ;
		if (ft_strchr("23", s->map[(int)r->pos_x][(int)r->pos_y]))
		{
			if ((int)r->pos_x != r->sprite_x && (int)r->pos_y != r->sprite_y)
				r->spritecount = r->spritecount + 1;
			if (r->sprite_x == -1 && r->sprite_y == -1)
			{
				r->sprite_x = r->pos_x;
				r->sprite_y = r->pos_y;
			}
		}
	}
}

static t_ray	*rayfactory(struct s_mlxvars *s, int castangle)
{
	t_ray *r;

	r = malloc(sizeof(t_ray));
	if (r == NULL)
		quit("Error:\nCouldn't allocate memory while raycasting.", s);
	r->path_cos = cos(degtorad(castangle)) / DATA_RAYPRECISION;
	r->path_sin = sin(degtorad(castangle)) / DATA_RAYPRECISION;
	r->pos_x = s->player_x;
	r->pos_y = s->player_y;
	r->sprite_x = -1;
	r->sprite_y = -1;
	r->spritecount = 0;
	r->spritenow = 0;
	throwray(r, s);
	r->distance = pt(s->player_x - r->pos_x, s->player_y - r->pos_y);
	r->wallheight = voidtile(r->pos_x, r->pos_y, s) ? 0 : (int)((s->res_y / 2) /
			(r->distance * cos(degtorad(castangle - s->playerdir))));
	return (r);
}

void			doraycast(struct s_mlxvars *s)
{
	t_ray	*r;
	int		i;
	double	castangle;

	i = s->res_x * DATA_OVERSCAN;
	castangle = s->playerdir - (DATA_FOV / 2);
	while (i > 0)
	{
		r = rayfactory(s, castangle);
		drawcwf(i, r, castangle, s);
		free(r);
		castangle = castangle +
			((double)DATA_FOV / (double)s->res_x) * s->renderprecision;
		i = i - s->renderprecision;
	}
}
