/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   linkedlist.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/01/19 17:18:57 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "linkedlist.h"
#include <stdlib.h>
#include "libft.h"

t_listitem	*additem(t_listitem *first, char *content)
{
	t_listitem *temp1;
	t_listitem *temp2;

	temp1 = malloc(sizeof(t_listitem));
	temp1->content = ft_strdup(content);
	temp1->next = NULL;
	if (first == NULL)
		first = temp1;
	else
	{
		temp2 = first;
		while (temp2->next != NULL)
			temp2 = temp2->next;
		temp2->next = temp1;
	}
	return (first);
}

void		freelist(t_listitem *first)
{
	t_listitem	*temp;

	while (first != NULL)
	{
		temp = first;
		first = first->next;
		free(temp);
	}
}
