/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   cub3d.h                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/09/11 15:05:20 by 😖            #+#    #+#                 */
/*   Updated: 2021/05/31 14:09:24 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUB3D_H
# define CUB3D_H

# define DATA_FOV			80
# define DATA_PRECISION		4
# define DATA_RAYPRECISION	56
# define DATA_MOVESPEED		0.15
# define DATA_LOOKSPEED		2
# define DATA_KNOCKBACK		6.5
# define DATA_DAMAGE		0.15
# define DATA_HEAL			0.25
# define DATA_SPRITEWIDTH	0.012
# define DATA_OVERSCAN		1.15
# define DATA_HPWIDTH		20
# define DATA_HPRED			0x00c04d4d
# define DATA_HPGREEN		0x0052a053
# define DATA_STAR			0x00f1f177
# define DATA_MAP_SIZE		2500
# define DATA_TEX_SIZE		64
# define DATA_SPRITE2_PATH	"./textures64/honeyjar.png"

# define KEY_PRESSED		2
# define KEY_RELEASED		3
# define CURSOR_MOVED		6
# define QUIT_REQUESTED		17

# define BTN_FORWARD		13
# define BTN_BACKWARD		1
# define BTN_LEFT			0
# define BTN_RIGHT			2
# define BTN_FORWARD_ALT	126
# define BTN_BACKWARD_ALT	125
# define BTN_LEFT_ALT		123
# define BTN_RIGHT_ALT		124
# define BTN_NUM_1			83
# define BTN_NUM_2			84
# define BTN_NUM_3			85
# define BTN_NUM_4			86
# define BTN_NUM_5			87
# define BTN_NUM_6			88
# define BTN_NUM_7			89
# define BTN_NUM_8			91
# define BTN_NUM_9			92
# define BTN_ESC			53
# define BTN_PUP			116
# define BTN_PDOWN			121
# define BTN_HEAL			36
# define BTN_HEAL_NUM		82

# define NORTH				180
# define EAST				90
# define SOUTH				0
# define WEST				270
# define SPRITE1			271
# define SPRITE2			272

# define COLOUR_R			2
# define COLOUR_G			1
# define COLOUR_B			0
# define COLOUR_T			3

# define DEBUGLOG			0

# include "libft.h"

union			u_colour
{
	unsigned int	mlxformat;
	unsigned char	rgbt[4];
};

typedef struct	s_image
{
	void				*img;
	void				*addr;
	int					bpp;
	int					linelength;
	int					endian;
	union u_colour		brush;
	int					x;
	int					y;
}				t_image;

typedef struct	s_ray
{
	double	pos_x;
	double	pos_y;
	double	path_cos;
	double	path_sin;
	double	distance;
	int		wallheight;
	int		side;
	int		facing;
	int		sprite_x;
	int		sprite_y;
	int		spritecount;
	int		spritenow;
}				t_ray;

struct			s_mlxvars
{
	void				*mlx;
	void				*window;
	t_image				*fb1;
	int					imgfile;
	int					mapfile;

	t_image				*northtex;
	t_image				*southtex;
	t_image				*westtex;
	t_image				*easttex;
	t_image				*sprite1tex;
	t_image				*sprite2tex;

	int					renderprecision;
	int					columnsleft;
	double				spritecast;

	int					viewmoved;
	int					settingcount;
	int					res_x;
	int					res_y;
	unsigned int		ccolour;
	unsigned int		fcolour;

	int					map_width;
	int					map_height;
	char				**map;

	int					spawn_x;
	int					spawn_y;
	int					spawndir;
	double				player_x;
	double				player_y;
	int					playerdir;
	int					walkforward;
	int					walkbackward;
	int					walkleft;
	int					walkright;
	double				health;
};

void			handlearguments(struct s_mlxvars *s, int argc, char **argv);
unsigned int	rgbt(unsigned char r, unsigned char g, unsigned char b,
						unsigned char t);
void			parse(struct s_mlxvars *s);
int				rendernextframe(struct s_mlxvars *s);
int				buttonpress(int button, struct s_mlxvars *s);
int				buttonrelease(int button, struct s_mlxvars *s);
int				mousemove(int x, int y, struct s_mlxvars *s);
void			quit(char *message, struct s_mlxvars *s);
int				quitrequested(struct s_mlxvars *s);

t_image			*imagefactory(struct s_mlxvars *s, int x, int y);
unsigned int	eyedropper(t_image *image, int x, int y);
void			drawpixel(t_image *image, int x, int y);
void			drawverticalline(t_image *image, int x, int start, int length);
void			loadtexture(char *path, int face, struct s_mlxvars *s);
double			degtorad(double deg);
int				fixangle(int deg);
void			walkforward(struct s_mlxvars *s);
void			walkright(struct s_mlxvars *s);
void			walkleft(struct s_mlxvars *s);
void			walkbackward(struct s_mlxvars *s);
void			bumpwall(struct s_mlxvars *s, double oldx, double oldy);
void			doraycast(struct s_mlxvars *s);
int				wallfacing(int side, int castangle);
t_image			**textureforface(int face, struct s_mlxvars *s);
int				voidtile(int pos1, int pos2, struct s_mlxvars *s);
void			setbrush(t_image *image, unsigned int colour, double health);
void			parsespawn(struct s_mlxvars *s, int y);
int				drawcwf(int x, t_ray *r, int castangle, struct s_mlxvars *s);
void			drawhealthbar(struct s_mlxvars *s);
void			doscreenshot(struct s_mlxvars *s);
void			freesplit(int length, struct s_ft_split *split);
void			parseresolution(char *line, struct s_mlxvars *s);
void			settexture(char *line, struct s_mlxvars *s);
void			setfccolour(char *line, struct s_mlxvars *s);
void			parsesettings(struct s_mlxvars *s);
int				dofloodfill(struct s_mlxvars *s);
void			mapclean(struct s_mlxvars *s);
void			mapclean2(struct s_mlxvars *s);
void			multiplespawncheck(char *mapstring, struct s_mlxvars *s);
double			pt(double x, double y);
void			parseplayerdir(struct s_mlxvars *s);

#endif
