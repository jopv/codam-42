/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   autoindex.dart                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: 😖       <😖      @student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/02/23 19:04:03 by 😖            #+#    #+#                 */
/*   Updated: 2021/02/23 19:04:03 by 😖            ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

import 'dart:io';
import 'dart:async';

Future main() async
{
	var nginxconfig = File('/etc/nginx/sites-available/ft_server.conf');

	var sc = SecurityContext();
	sc.useCertificateChain("/etc/ssl/certs/opensslselfsign.crt");
	sc.usePrivateKey("/etc/ssl/private/opensslselfsign.key");
	var webserver = await HttpServer.bindSecure('localhost', 4047, sc);
	Process.run("service", ["nginx", "start"]);
	Process.run("service", ["php7.3-fpm", "start"]);
	Process.run("service", ["mysql", "start"]);
	print("Server actief, ga naar https://localhost");
	await for (HttpRequest r in webserver)
	{
		r.response.headers.contentType = ContentType("text", "html", charset: "utf-8");
		r.response.write("""
				<html>
				<head><title>ft_server</title></head>
				<style type=\"text/css\">
					body
					{
						font-family: font-family: \"Times New Roman\", \"Liberation Serif\", serif;
					}
					p
					{
						font-size: 14pt;
					}
				</style>
				<body>
				<h1>ft_server autoindex toggle</h1>
			""");
		nginxconfig.readAsString().then((String configuration)
		{
			if (configuration.contains("autoindex on;"))
			{
				nginxconfig.writeAsString(configuration.replaceAll("autoindex on;", "autoindex off;"));
				Process.run("service", ["nginx", "reload"]);
				r.response.write("""
							<style type=\"text/css\">
								body
								{
									background: linear-gradient(to bottom, white, #c04d4d) fixed;
								}
							</style>
							<p>🇬🇧 Autoindex disabled, reload this page to enable it again.</p>
							<p>🇳🇱 Autoindex uitgezet, herlaad deze pagina om het weer aan te zetten.</p>
							<p>🇫🇷 Autoindex désactivé, rechargez cette page pour la réactiver.</p>
						""");
			}
			else if (configuration.contains("autoindex off;"))
			{
				nginxconfig.writeAsString(configuration.replaceAll("autoindex off;", "autoindex on;"));
				Process.run("service", ["nginx", "reload"]);
				r.response.write("""
							<style type=\"text/css\">
								body
								{
									background: linear-gradient(to bottom, white, #52a053) fixed;
								}
							</style>
							<p>🇬🇧 Autoindex enabled, reload this page to disable it again.</p>
							<p>🇳🇱 Autoindex aangezet, herlaad deze pagina om het weer uit te zetten.</p>
							<p>🇫🇷 Autoindex activé, rechargez cette page pour la désactiver.</p>
						""");
			}
			else
				r.response.write("<p>No autoindex setting found in the Nginx config.</p>");
			r.response.write("</body></html>");
			r.response.close();
		});
	}
}
