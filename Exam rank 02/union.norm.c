/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   union.norm.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: fvernooi <fvernooi@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/04/09 18:06:53 by fvernooi      #+#    #+#                 */
/*   Updated: 2021/04/09 18:07:55 by fvernooi      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

#define FORWARD		1
#define BACKWARD	0

int	char_count(char *string, char to_count, int start, int direction)
{
	int	count;
	int	i;

	count = 0;
	i = start;
	while (i >= 0 && string[i] != '\0')
	{
		if (string[i] == to_count)
			count++;
		if (direction)
			i++;
		else
			i--;
	}
	return (count);
}

int	main(int argc, char **argv)
{
	int	i;

	if (argc == 3)
	{
		i = 0;
		while (argv[1][i] != '\0')
		{
			if (char_count(argv[1], argv[1][i], i, BACKWARD) == 1)
				write(STDOUT_FILENO, argv[1] + i, 1);
			i++;
		}
		i = 0;
		while (argv[2][i] != '\0')
		{
			if (char_count(argv[1], argv[2][i], 0, FORWARD) == 0 &&
				char_count(argv[2], argv[2][i], i, BACKWARD) == 1)
				write(STDOUT_FILENO, argv[2] + i, 1);
			i++;
		}
	}
	write(STDOUT_FILENO, "\n", 1);
	return (0);
}
