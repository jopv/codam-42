#include <unistd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdbool.h>

int	ft_isdigit(char c)
{
	return (c >= '0' && c <= '9');
}

int	ft_strlen(char const *string)
{
	int length = 0;
	while (string[length] != '\0')
		length++;
	return (length);
}

size_t	ft_dlen(int nb)
{
	if (nb == 0)
		return (2);
	if (nb < 0)
		nb = -nb;
	size_t len = 1;
	for (int measure = nb; measure != 0; measure /= 10)
		len++;
	return (len);
}
size_t	ft_xlen(unsigned int nb)
{
	if (nb == 0)
		return (2);
	size_t len = 1;
	for (unsigned int measure = nb; measure != 0; measure /= 16)
		len++;
	return (len);
}

unsigned int	ft_atoi(const char *str)
{
	unsigned int result = 0;
	for (int i = 0; ft_isdigit(str[i]); i++)
		result = result * 10 + str[i] - '0';
	return (result);
}

char	*ft_strdup(char *todup)
{
	char *toreturn = malloc(ft_strlen(todup) + 1);
	int i;
	for (i = 0; todup[i] != '\0'; i++)
		toreturn[i] = todup[i];
	toreturn[i] = '\0';
	return (toreturn);
}

static const char *set = "0123456789abcdef";
char	*ft_itoa(int nb)
{
	if (nb == 0)
		return (ft_strdup("0"));
	if (nb < 0)
		nb = -nb;
	size_t len = ft_dlen(nb);
	char *toreturn = malloc(len + 1);
	for (size_t i = len - 2; nb != 0; i--)
	{
		toreturn[i] = set[nb % 10];
		nb /= 10;
	}
	toreturn[len] = '\0';
	return (toreturn);
}
char *ft_itoa_x(unsigned int nb)
{
	if (nb == 0)
		return (ft_strdup("0"));
	size_t len = ft_xlen(nb);
	char *toreturn = malloc(len + 1);
	for (size_t i = len - 2; nb != 0; i--)
	{
		toreturn[i] = set[nb % 16];
		nb /= 16;
	}
	toreturn[len] = '\0';
	return (toreturn);
}

int ft_printpadding(int minwidth, int length, int precision, bool negative)
{
	int printed = 0;
	precision = (precision > length) ? precision - length : 0;
	for (int i = minwidth - length - precision - negative; i > 0; i--)
		printed += write(STDOUT_FILENO, " ", 1);
	if (negative)
		printed += write(STDOUT_FILENO, "-", 1);
	while (precision-- > 0)
		printed += write(STDOUT_FILENO, "0", 1);
	return (printed);
}

int ft_printf(char const *fmt, ...)
{
	va_list	variables;
	va_start(variables, fmt);
	size_t length = 0;
	for(; *fmt != '\0'; ++fmt)
	{
		if (*fmt != '%')
			length += write(STDOUT_FILENO, fmt, 1);
		else
		{
			if (*++fmt == '\0') goto quit;
			int minwidth = ft_atoi(fmt);
			while (ft_isdigit(*fmt))
				if (*++fmt == '\0') goto quit;

			int precision = 0;
			bool blank = false;
			if (*fmt == '.')
			{
				if (*++fmt == '\0') goto quit;
				precision = ft_atoi(fmt);
				blank = (precision == 0);
				while (ft_isdigit(*fmt))
					if (*++fmt == '\0') goto quit;
			}

			if (*fmt == 's')
			{
				char *string = va_arg(variables, char *);
				bool wasnull = (string == NULL);
				if (wasnull)
					string = ft_strdup("(null)");
				int strlen = (precision != 0 && precision < ft_strlen(string)) || blank ?
							 precision : ft_strlen(string);
				length += ft_printpadding(minwidth, strlen, 0, false);
				length += write(STDOUT_FILENO, string, strlen);
				if (wasnull)
					free(string);
			}
			else
			{
				char *number;
				bool negative = false;
				if (*fmt == 'd' || *fmt == 'i')
				{
					int d = va_arg(variables, int);
					number = ft_itoa(d);
					negative = (d < 0);
				}
				else if (*fmt == 'x')
					number = ft_itoa_x(va_arg(variables, unsigned int));
				else goto quit;
				int numlen = (*number == '0' && blank) ? 0 : ft_strlen(number);
				length += ft_printpadding(minwidth, numlen, precision, negative);
				length += write(STDOUT_FILENO, number, numlen);
				free(number);
			}
		}
	}
	quit:
	va_end(variables);
	return(length);
}
