#include <unistd.h>
#include <stdbool.h>

int char_count(char *string, char to_count, int start, bool forward)
{
	int count = 0;
	for (int i = start; i >= 0 && string[i] != '\0'; i += forward ? 1 : -1)
		if (string[i] == to_count)
			count++;
	return (count);
}

int main(int argc, char** argv)
{
	if (argc == 3)
		for (int i = 0; argv[1][i] != '\0'; i++)
			if (char_count(argv[1], argv[1][i], i, false) == 1 &&
				char_count(argv[2], argv[1][i], 0, true) > 0)
				write(STDOUT_FILENO, argv[1] + i, 1);
	write(STDOUT_FILENO, "\n", 1);
	return (0);
}
