package net.jopv.codamjobinterview;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Contact extends RealmObject
{
    @PrimaryKey private String phoneNumber;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String homeAddress;

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }
    public String getPhoneNumber()
    {
        return this.phoneNumber;
    }
    public void setName(String firstName, String lastName)
    {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    public String getFirstName()
    {
        return this.firstName;
    }
    public String getLastName()
    {
        return this.lastName;
    }
    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }
    public void setHomeAddress(String homeAddress)
    {
        this.homeAddress = homeAddress;
    }
    public String getEmailAddress()
    {
        return this.emailAddress;
    }
    public String getHomeAddress()
    {
        return this.homeAddress;
    }
    public Contact() {}
}
