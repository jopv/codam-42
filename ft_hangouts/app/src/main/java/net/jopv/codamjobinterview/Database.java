package net.jopv.codamjobinterview;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class Database
{
    Realm realmInstance;

    public Database(Realm realmInstance)
    {
        this.realmInstance = realmInstance;
    }

    public void saveContact(final Contact contact)
    {
        realmInstance.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realmInstance)
            {
                realmInstance.copyToRealm(contact);
            }
        });
    }

    public ArrayList<String> getNames()
    {
        ArrayList<String> names = new ArrayList<>();
        RealmResults<Contact> contacts = realmInstance.where(Contact.class).findAll();
        for (Contact c : contacts)
            names.add(c.getFirstName() + " " + c.getLastName());
        return (names);
    }
}
